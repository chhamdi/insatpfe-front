import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AcademicYearDto } from 'src/app/dto/academicYear.dto';
import { AcademicYearService } from 'src/app/services/academic-year.service';

@Component({
  selector: 'app-academic-year',
  templateUrl: './academic-year.component.html',
  styleUrls: ['./academic-year.component.scss']
})
export class AcademicYearComponent implements OnInit {

  academicYears : any
  isLoading:Boolean
  totalsize=0
  selectedAcademicYear:any;
  updatedAcademicYear:AcademicYearDto;
  createdAcademicYear:AcademicYearDto;



  constructor(private academicYearService: AcademicYearService, private modalService: NgbModal ) {
    this.isLoading=true
   }

  ngOnInit(): void {
    this.getListAcademicYears(1,10)
  }


  getListAcademicYears(page,limit){
    this.academicYearService.getListAcadameciYears(page,limit).subscribe((years)=>{
      this.academicYears = years.data;
      this.totalsize=years.totalCount; 
      this.isLoading=false
    },
    (error)=>{
      console.log("error"+error);   
    }
    );
  }

  openModalDeleteAcademicYear( modal ,AcademicYear) {
    this.modalService.open( modal, { size : 'lg' } );
    this.selectedAcademicYear = AcademicYear;
  }


  deleteAcademicYear(id:string, modal){
    this.academicYearService.deleteAcademicYear(id).subscribe((response)=>{
      if(response.affected == 1){
        this.modalService.dismissAll()
        this.modalService.open( modal, { size : 'lg' } );
        this.getListAcademicYears(1,10);
      }
    console.log(response);
    })
  }

  addAcademicYear(formulaire:any,modal){
    this.createdAcademicYear=new AcademicYearDto(formulaire.dYear+"-"+formulaire.eYear)
    this.academicYearService.addAcademicYear(this.createdAcademicYear).subscribe((response)=>{
    console.log("AcademicYear"+response);
    this.modalService.dismissAll()
     this.modalService.open( modal, { size : 'lg' } );
     this.getListAcademicYears(1,10);
    })   
  }

  updateAcademicYear(id:string,formulaire:AcademicYearDto,modal){
    this.academicYearService.updateAcademicYear(id,formulaire).subscribe((response)=>{
    this.modalService.dismissAll()
     this.modalService.open( modal, { size : 'lg' } );
     this.getListAcademicYears(1,10);
    })   
  }

  openModalAddAcademicYear( modal ) {
    this.modalService.open( modal, { size : 'lg' } );
  }  

  openModalUpdateAcademicYear( modal ,academicYear : AcademicYearDto) {
    this.modalService.open( modal, { size : 'lg' } );
    this.selectedAcademicYear = academicYear;
  }

  search(form,page,limit){
    this.academicYearService.search(form.name,page,limit).subscribe(
      (years)=>{
        console.log(years)
        this.academicYears = years.data;
       this.totalsize=years.totalCount; 
      },(error)=>{
          console.log("err"+error);        
      })
  }

  clearSearch(form){
    form.reset()
    this.getListAcademicYears(1,10) 
  }

}
