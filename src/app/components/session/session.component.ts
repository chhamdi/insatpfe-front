import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { error } from 'protractor';
import { SessionDto } from 'src/app/dto/session.dto';
import { AcademicYearService } from 'src/app/services/academic-year.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {

  sessions : any
  isLoading:Boolean
  totalsize=0
  public role : string;
  years:any;
  selectedOption: any;
  selectedSession: any;

  constructor(private sessionService: SessionService, private academicYearService: AcademicYearService,  private modalService: NgbModal ) {
    this.isLoading=true
   }

  ngOnInit(): void {
    this.role = localStorage.getItem('role');
    this.getListSessions(1,10);
    this.academicYearService.getListAcadameciYears(1,20).subscribe(
      (years)=>{
        this.years=years.data
      }
    )
  }

  getListSessions(page,limit){
    this.sessionService.getListSessions(page,limit).subscribe((sessions)=>{
          this.totalsize=sessions.totalCount
          this.sessions=sessions.data;
          this.isLoading=false
    },(error)=>{
      console.log("err"+error);
    })
  }

  search(form,page,limit){
   
      let uri:string="?page="+page+"&limit="+limit
      if (form.startDate!=""){
        uri=uri+"&startDate="+form.startDate
      }
      if (form.endDate!=""){
        uri=uri+"&endDate="+form.endDate
      }
     
      console.log(uri)

    this.sessionService.search(uri).subscribe(
      (sessions)=>{
        console.log(sessions)
        this.sessions=sessions.data;
       this.totalsize=sessions.totalCount; 
      },(error)=>{
          console.log("err"+error);    
      })
  }

  clearSearch(form){
    form.reset()
    this.getListSessions(1,10)
  }

  openModalAddSession( modal ) {
    this.modalService.open( modal, { size : 'lg' } );
  }  

  
  addSession(formulaire:SessionDto,modal){
    this.sessionService.addSession(formulaire).subscribe((response)=>{
    console.log("session"+response);
    this.modalService.dismissAll()
     this.modalService.open( modal, { size : 'lg' } );
     this.getListSessions(1,10);
    })   
  }

  OnOptionSelected(option){
    this.selectedOption=option
  }

  openModalDeleteSession( modal ,session) {
    this.modalService.open( modal, { size : 'lg' } );
    this.selectedSession = session;
  }

  deleteRoom(id:string, modal){
    this.sessionService.deleteSession(id).subscribe((response)=>{
      
        this.modalService.dismissAll()
        this.modalService.open( modal, { size : 'lg' } );
        this.getListSessions(1,10);
      
    console.log(response);
    })
  }

}
