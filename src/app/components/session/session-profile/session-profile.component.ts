import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-session-profile',
  templateUrl: './session-profile.component.html',
  styleUrls: ['./session-profile.component.scss']
})
export class SessionProfileComponent implements OnInit {

sessionId :String;
 session:any;
 totalsize=0
 
  soutenances: any;
  isLoading:Boolean

  constructor(private route: ActivatedRoute,private sessionService: SessionService) {
    this.isLoading=true
   }


  ngOnInit(): void {
    this.route.paramMap
    .subscribe(params => {
      this.sessionId = params.get('id');
      this.sessionService.getSessionById(this.sessionId)
      .subscribe(
        response => {
        this.session=response; 
        this.isLoading=false
        console.log(this.session) 

      
        this.sessionService.getSessionSoutenancesById(this.sessionId).subscribe(
          (response) => {
            this.soutenances=response; 
            this.totalsize=this.soutenances.totalCount; 
            console.log(this.soutenances)
            console.log(this.totalsize)

           }
        )




        })
        });
        
  }


}
