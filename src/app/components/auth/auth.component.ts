import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from 'src/app/dto/user.dto';
import { AdminService } from 'src/app/services/admin.service';
import { AuthService } from 'src/app/services/auth.service';
import { StudentService } from 'src/app/services/etudiant.service';
import { TeacherService } from 'src/app/services/teacher.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  showError = false;
  constructor(private authService : AuthService,private router: Router,
    private studentService : StudentService,
    private teacherService : TeacherService,
    private adminService : AdminService
    ) { }

  ngOnInit(): void {
  }


  login(credentials){
    console.log("value"+JSON.stringify(credentials));
    
    this.authService.login(credentials).subscribe((response)=>{
      if(response.access_token)
        {
          this.showError=false;
          localStorage.setItem("token",response.access_token);
          localStorage.setItem("role",response.role);
          localStorage.setItem("id",response.id);
          if(response.role == "admin")
          {
            this.getProfileAdmin(response.id)
            this.router.navigate(['/profile/admin/'+response.id])
          }
          if(response.role == "enseignant")
          {
            this.getProfileTeacher(response.id)
            this.router.navigate(['/profile/enseignant/'+response.id])
          }
          else if(response.role =="etudiant")
            {
              this.getProfileStudent(response.id)
              this.router.navigate(['/profile/etudiant/'+response.id])
            }                       
        }
      else
        this.showError=true
    })
  }



  getProfileStudent(id){
    this.studentService.getStudentById(id).subscribe((user)=>{
      localStorage.setItem("firstname",user.firstname);
    })
  }

  getProfileTeacher(id){
    this.teacherService.getTeacherById(id).subscribe((teacher)=>{
      localStorage.setItem("firstname",teacher.firstname);
    })
  }

  getProfileAdmin(id){
    this.adminService.getAdminById(id).subscribe((admin)=>{
      localStorage.setItem("firstname",admin.firstname);
    })
  }



}
