import { FileItem, FileUploader } from 'ng2-file-upload';
import { Component, OnInit } from '@angular/core';
import { TeacherDto } from 'src/app/dto/teacher.dto';
import { TeacherService } from 'src/app/services/teacher.service';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StudentDto } from 'src/app/dto/student.dto';
@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss']
})
export class TeacherComponent implements OnInit {

  teachers: any
  isLoading: Boolean
  totalsize = 0
  uploader: FileUploader;
  public role: string;
  selectedTeacher: StudentDto;

  departments: String[] = ["Génie Informatique et Mathématiques", "Génie Physique et Instrumentation",
    "Génie Biologique et de Chimie", "Sciences Sociales, Langues et Formation Générale"];
  depSelected: String = "Génie Informatique et Mathématiques";


  constructor(private teacherService: TeacherService, private modalService: NgbModal) {
    this.isLoading = true
  }

  ngOnInit(): void {
    this.role = localStorage.getItem('role');
    this.getListTeachers(1, 10);
    this.initFileUploader();
  }

  initFileUploader(): void {
    this.uploader = this.createUploader('enseignants/import');
  }

  createUploader(url: string): FileUploader {
    const token = localStorage.getItem('token');
    const uploader = new FileUploader(
      {
        url: environment.apiUrl + url,
        authToken: `Bearer ${token}`
      });

    //uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
    //  const year = this.years['data'].find(y => y.name == this.yearSelected)
    //  form.append('academicYear', year.id);
    //};

    uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <TeacherDto[]>JSON.parse(response)//<any>JSON.parse(response);
      this.teachers = this.teachers.concat(ajaxResponse);
    };
    return uploader;
  }

  uploadFile(): void {
    this.uploader.uploadAll();
  }

  getListTeachers(page, limit) {
    this.teacherService.getListTeachers(page, limit).subscribe((teachers) => {
      this.teachers = teachers.data;
      this.totalsize = teachers.totalCount;
      this.isLoading = false
    })
  }

  changeDep(dep) {
    this.depSelected = dep;
  }


  search(form, page, limit) {
    console.log(form)

    let uri: string = "?page=" + page + "&limit=" + limit
    if (form.cin != "" && form.cin != undefined) {
      uri = uri + "&cin=" + form.cin
    }
    if (form.name != "") {
      uri = uri + "&name=" + form.name
    }
    if (form.firstname != "") {
      uri = uri + "&firstname=" + form.firstname
    }
    if (form.email != "") {
      uri = uri + "&email=" + form.email
    }
    if (form.telephone != "" && form.telephone != undefined) {
      uri = uri + "&telephone=" + form.telephone
    }


    console.log(uri)

    this.teacherService.search(uri).subscribe(
      (teachers) => {
        this.teachers = teachers.data;
        console.log("searched", this.teachers);

        this.totalsize = teachers.totalCount;
      }, (error) => {
        console.log("err" + error);
      })
  }

  clearSearch(form) {
    form.reset()
    this.getListTeachers(1, 10)
  }

  openModalAddTeacher(modal) {
    this.modalService.open(modal, { size: 'lg' });
  }

  openModalDeleteTeacher(modal, teacher: StudentDto) {
    this.modalService.open(modal, { size: 'lg' });
    this.selectedTeacher = teacher;
  }


  openModalUpdateTeacher(modal, teacher: StudentDto) {
    this.modalService.open(modal, { size: 'lg' });
    this.selectedTeacher = teacher;
  }

  addTeacher(formulaire: TeacherDto, modal) {
    let password = Math.random().toString(36).slice(-8);
    formulaire.password = password + "";
    formulaire.cin = parseInt(formulaire.cin + "");
    formulaire.passeport = parseInt(formulaire.passeport + "");
    formulaire.telephone = parseInt(formulaire.telephone + "")
    this.teacherService.addTeacher(formulaire).subscribe((teacher) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getListTeachers(1, 10);
    })
  }
  updateTeacher(id: string, formulaire: TeacherDto, modal) {
    formulaire.password = this.selectedTeacher.password;
    this.teacherService.updateTeacher(id, formulaire).subscribe((response) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getListTeachers(1, 10);
    })
  }
  deleteTeacher(id: string, modal) {
    this.teacherService.deleteTeacher(id).subscribe((response) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getListTeachers(1, 10);
    })
  }

}
