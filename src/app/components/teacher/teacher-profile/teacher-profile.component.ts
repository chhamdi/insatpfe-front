import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeacherEntity } from 'src/app/entities/Teacher.entity';
import { TeacherService } from 'src/app/services/teacher.service';
import { InternshipComponent } from '../../internship/internship.component';
import { InternshipEntity } from '../../../entities/Internship.entity';
import { StateEnum } from '../../../entities/enums/stage-state.enum';

@Component({
  selector: 'app-teacher-profile',
  templateUrl: './teacher-profile.component.html',
  styleUrls: ['./teacher-profile.component.scss']
})
export class TeacherProfileComponent implements OnInit {

  teacherId :String;
  teacher:any;
 
  internships: any;
  refusedInternships: any[]=[];
  validInternships: any[]=[];
  waitingInternships: any[]=[];


  soutenances: any;

  presidentSoutenances: any[]=[];
  reviewerSoutenances: any[]=[];
  supervisorSoutenances: any[]=[];

  isLoading:boolean
  internshipIsLoading:boolean

  constructor(private route: ActivatedRoute,private teacherService: TeacherService,) { 
    this.isLoading=true
    this.internshipIsLoading=true
  }

  ngOnInit(): void {

    this.route.paramMap
    .subscribe(params => {
      this.teacherId = params.get('id');
      this.teacherService.getTeacherById(this.teacherId)
      .subscribe(
        response => {
        this.teacher=response; 
        this.isLoading=false
        console.log(this.teacher) 

        this.teacherService.getTeacherInternshipsById(this.teacherId).subscribe(
          (response) => {
           this.internships=response; 
            this.internshipIsLoading=false
            console.log(this.internships) 

            for (var val of this.internships.interships) {

              if (val.soutenance){
                this.supervisorSoutenances.push(val.soutenance)
              }
              if (val.state==StateEnum.WAITING) 
              {
                this.waitingInternships.push(val);
              } 
              else if (val.state==StateEnum.VALID)
              {
                this.validInternships.push(val);
              }
              else if (val.state==StateEnum.REFUSED) 
              {
                this.refusedInternships.push(val);
              }

            }
          }
        )

      


        this.teacherService.getTeacherPresidentSoutenancesById(this.teacherId).subscribe(
          response => {
            this.soutenances=response; 
            console.log(this.soutenances) 
            this.presidentSoutenances=this.soutenances.soutenances
          }
        )

        this.teacherService.getTeacherReviewerSoutenancesById(this.teacherId).subscribe(
          response => {
            this.soutenances=response; 
            console.log(this.soutenances) 
            this.reviewerSoutenances=this.soutenances.soutenances
          }
        )




        })
        });
        
    }

}
