import { PaginatedResultDto } from './../../dto/pagination.result.dto';
import { AcademicYearService } from 'src/app/services/academic-year.service';
import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AcademicYearEntity } from 'src/app/entities/AcademicYear.entity';
import { StudentEntity } from 'src/app/entities/Student.entity';
import { StudentService } from 'src/app/services/etudiant.service';
import { StudentDto } from '../../dto/student.dto';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-etudiant',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {

  students: any;
  uploader: FileUploader;
  selectedStudent: StudentDto;
  years: PaginatedResultDto;
  yearSelected: String;
  isLoading: Boolean;
  totalsize = 0

  constructor(
    private studentService: StudentService,
    private academicYearService: AcademicYearService,
    private modalService: NgbModal
  ) {
    this.isLoading = true
    this.years = new PaginatedResultDto();
  }

  ngOnInit(): void {

    this.academicYearService.getListAcadameciYears(1, 20).subscribe(years => {
      this.years = years;
      this.isLoading = false;

      this.yearSelected = years.data[0].name;
      this.getLisSudents(1, 10);
    },
      (error) => {
        console.log("error" + error);
        this.isLoading = false;
      }
    );
    this.initFileUploader();

  }

  initFileUploader(): void {
    this.uploader = this.createUploader('etudiants/import');
  }

  createUploader(url: string): FileUploader {
    const token = localStorage.getItem('token');
    const uploader = new FileUploader(
      {
        url: environment.apiUrl + url,
        authToken: `Bearer ${token}`
      });

    uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
      const year = this.years['data'].find(y => y.name == this.yearSelected)
      form.append('academicYear', year.id);
    };

    uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    uploader.onSuccessItem = (item, response, status) => {
      const ajaxResponse = <StudentDto[]>JSON.parse(response)//<any>JSON.parse(response);
      this.students = this.students.concat(ajaxResponse);
    };
    return uploader;
  }

  uploadFile(): void {
    this.uploader.uploadAll();
  }

  changeYear(year: String) {
    this.isLoading = true;
    this.yearSelected = year;
    this.getLisSudents(1, 10);
  }

  openModalAddStudent(modal) {
    this.modalService.open(modal, { size: 'lg' });
  }

  openModalDeleteStudent(modal, student: StudentDto) {
    this.modalService.open(modal, { size: 'lg' });
    this.selectedStudent = student;
  }


  openModalUpdateStudent(modal, student: StudentDto) {
    this.modalService.open(modal, { size: 'lg' });
    this.selectedStudent = student;
  }

  deleteSudent(id: string, modal) {
    this.studentService.deleteStudent(id).subscribe((response) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getLisSudents(1, 10);
    })
  }


  getLisSudents(page, limit) {
    let id = this.getIdYear();
    console.log("Year=" + this.yearSelected);
    console.log("Year=" + id);

    this.academicYearService.getStudentsByAcYear(id, page, limit).subscribe((students) => {
      this.students = students.data
      console.log(this.students)
      this.totalsize = students.totalCount
      this.isLoading = false;
    },
      (error) => {
        console.log("error" + error);
        this.isLoading = false;
      }
    );
  }

  addStudent(formulaire: StudentDto, modal) {
    const year = this.years['data'].find(y => y.name == this.yearSelected)
    formulaire.password = formulaire.inscription_number + "";
    formulaire.academicYear = year.id;
    formulaire.cin = parseInt(formulaire.cin + "");
    formulaire.passeport = parseInt(formulaire.passeport + "");
    formulaire.telephone = parseInt(formulaire.telephone + "")
    this.studentService.addStudent(formulaire).subscribe((student) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getLisSudents(1, 10);
    })

  }
  updateStudent(id: string, formulaire: StudentDto, modal) {
    formulaire.password = this.selectedStudent.password;
    this.studentService.updateStudent(id, formulaire).subscribe((response) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getLisSudents(1, 10);
    })
  }


  getIdYear(): Number {
    let year = this.years.data.find(s => s.name == this.yearSelected);
    return year.id;
  }

  search(form, page, limit) {
    console.log(form)

    let uri: string = "?page=" + page + "&limit=" + limit
    if (form.inscription_number != "") {
      uri = uri + "&inscription_number=" + form.inscription_number
    }
    if (form.name != "") {
      uri = uri + "&name=" + form.name
    }
    if (form.firstname != "") {
      uri = uri + "&firstname=" + form.firstname
    }
    if (form.section != "") {
      uri = uri + "&section=" + form.section
    }
    if (form.branch != "") {
      uri = uri + "&branch=" + form.branch
    }

    console.log(uri)

    this.studentService.search(uri).subscribe(
      (students) => {
        console.log(students)
        this.students = students.data;
        this.totalsize = students.totalCount;
      }, (error) => {
        console.log("err" + error);
      })
  }

  clearSearch(form) {
    form.reset()
    this.getLisSudents(1, 10)
  }
}
