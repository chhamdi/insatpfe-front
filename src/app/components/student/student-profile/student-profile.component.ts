import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StateEnum } from 'src/app/entities/enums/stage-state.enum';
import { StudentEntity } from 'src/app/entities/Student.entity';
import { StudentService } from 'src/app/services/etudiant.service';
import { InternshipService } from 'src/app/services/internship.service';
import { TeacherService } from 'src/app/services/teacher.service';

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.scss']
})
export class StudentProfileComponent implements OnInit {

  studentId :String;
  student:any;

  internshipToAdd:any;
 
  internships: any;
  soutenances: any;
  isLoading:Boolean;
  supervisor2:Boolean

  schooolSupervisors:any;
  selectedOption1:any;
  selectedOption2:any;

  constructor(private route: ActivatedRoute,
    private studentService: StudentService,
    private teacherService:TeacherService,
    private internshipService:InternshipService,
    private modalService: NgbModal) {
    this.isLoading=true
    this.supervisor2=false
    this.schooolSupervisors=[]
   }


  ngOnInit(): void {
    this.route.paramMap
    .subscribe(params => {
      this.studentId = params.get('id');
      this.studentService.getStudentById(this.studentId)
      .subscribe(
        response => {
        this.student=response; 
        this.isLoading=false
        console.log(this.student) 

        this.studentService.getStudentInternshipsById(this.studentId).subscribe(
          (response) => {
           this.internships=response; 
            console.log(this.internships) }
        )
        this.studentService.getStudentSoutenancesById(this.studentId).subscribe(
          response => {
            this.soutenances=response; 
            console.log(this.soutenances) }
        )
        })
        });      
  }

  addInternship(formulaire:any,modal){

    this.internshipToAdd= {
    "topic": "",
      "society": "",
      "societySupervisor": "",
      "schoolSupervisor1": "",
      "startDate": "",
      "endDate": "",
      "state": ""
    }

   // this.internshipToAdd.student=this.studentId
    this.internshipToAdd.topic=formulaire.topic
    this.internshipToAdd.society=formulaire.society

    this.internshipToAdd.societySupervisor=formulaire.societySupervisor
    this.internshipToAdd.schoolSupervisor1=this.selectedOption1;
    if (this.supervisor2===true) {
      this.internshipToAdd.schoolSupervisor2=this.selectedOption2
  }

    this.internshipToAdd.startDate=formulaire.startDate
    this.internshipToAdd.endDate=formulaire.endDate
    this.internshipToAdd.endDate=formulaire.endDate
    this.internshipToAdd.state=StateEnum.WAITING

    console.log( this.internshipToAdd)
    console.log( this.supervisor2)

  this.internshipService.addInternship(this.internshipToAdd).subscribe((response)=>{
    console.log("Ajouter Stage"+response);
    this.modalService.dismissAll()
    this.modalService.open( modal, { size : 'lg' } );
    }) 
  }

  OnOptionSelected1(option){
    this.selectedOption1=option
  }
  OnOptionSelected2(option){
    this.selectedOption2=option
  }

  openModalAddInternship( modal ) {
    this.modalService.open( modal, { size : 'lg' } );
  }
  
  
  getSupervisors(name){
    console.log("sssss",name)
    this.teacherService.getTeachers(name).subscribe(
      (response) => {
       this.schooolSupervisors=response; 
        console.log(this.schooolSupervisors) }
    )
  }



}
