import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SoutenanceService } from 'src/app/services/soutenance.service';

@Component({
  selector: 'app-soutenance-profile',
  templateUrl: './soutenance-profile.component.html',
  styleUrls: ['./soutenance-profile.component.scss']
})
export class SoutenanceProfileComponent implements OnInit {

  soutenanceId :String;
  soutenance:any;
 
  internship: any;
  soutenances: any;

  isLoading:boolean


  constructor(private route: ActivatedRoute,private soutenanceService: SoutenanceService) { 
    this.isLoading=true;
  }

  ngOnInit(): void {

    this.route.paramMap
    .subscribe(params => {
      this.soutenanceId = params.get('id');
      this.soutenanceService.getSoutenanceById(this.soutenanceId)
      .subscribe(
        response => {
        this.soutenance=response; 
        this.isLoading=false
        console.log(this.internship) 

        })

        });
        

}


downloadPdf(id,name,firstname){
  this.soutenanceService.downloadPdf(id).subscribe((result)=>{
    console.log(result.data);    
    let url = window.URL.createObjectURL(result);
    //window.open(url)
    console.log(url);
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = "soutenance-"+name+"-"+firstname+".pdf";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  })}
}






