import { SessionComponent } from './../session/session.component';
import { SessionService } from 'src/app/services/session.service';
import { PaginatedResultDto } from './../../dto/pagination.result.dto';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { SoutenanceDto } from 'src/app/dto/soutenance.dto';
import { AcademicYearEntity } from 'src/app/entities/AcademicYear.entity';
import { InternshipEntity } from 'src/app/entities/Internship.entity';
import { SoutenanceEntity } from 'src/app/entities/Soutenance.entity';
import { AcademicYearService } from 'src/app/services/academic-year.service';
import { InternshipService } from 'src/app/services/internship.service';
import { SoutenanceService } from 'src/app/services/soutenance.service';
import { RoomService } from 'src/app/services/room.service';
import { StudentService } from 'src/app/services/etudiant.service';

@Component({
  selector: 'app-soutenance',
  templateUrl: './soutenance.component.html',
  styleUrls: ['./soutenance.component.scss']
})
export class SoutenanceComponent implements OnInit {

  years: PaginatedResultDto //= ["2018-2019", "2017-2018", "2016-2017"];
  yearSelected : AcademicYearEntity;
  isLoading:Boolean;
  totalsize=0;
  selectedDate: Date;
  sessions: PaginatedResultDto

  internships : InternshipEntity[];
  allSessions:any=[];
  jurys:any=[];
  student:any;
  rooms:any=[];
  section:String;
  branch:String;
  soutenanceToAdd:any;

  sup1:String;
  sup2:String;

  soutenances:any=[]

  timeIsChosen:Boolean=false;
  studentFetched:Boolean=false;
  internshipFetched:Boolean=false;

  session:any;

  fInternship:any;
  fPresident:any;
  fReviewer:any;
  fRoom:any;

  constructor(
    private internshipService: InternshipService,
    private soutenanceService :SoutenanceService, 
    private academicYearService: AcademicYearService, 
    private modalService: NgbModal,
    private sessionService: SessionService,
    private studentService: StudentService,
    private roomService: RoomService,
    ) {
    this.isLoading=true

  }

 ngOnInit(): void {
   this.getListSoutenances(1,10);
   this.academicYearService.getListAcadameciYears(1,20).subscribe(years =>
    {
      this.years = years;
      this.yearSelected = years.data[0].name;
    },
    (error)=>{
      console.log("error"+error);   
      this.isLoading=false;
    }
    );
    this.sessionService.getListSessions(1,20).subscribe(sessions =>
      {
        this.sessions = sessions;
        this.allSessions=sessions.data
      },
      (error)=>{
        console.log("error"+error);   
        this.isLoading=false;
      }
      );
   //this.getAcYears();
 }


 search(form,page,limit){

    let uri:string="?page="+page+"&limit="+limit
    if (form.student!=""){
      uri=uri+"&student="+form.student
    }
    if (form.topic!=""){
      uri=uri+"&topic="+form.topic
    }
    if (form.president!=""){
      uri=uri+"&president="+form.president
    }
    if (form.supervisor!=""){
      uri=uri+"&supervisor="+form.supervisor
    }
    if (form.reviewer!=""){
      uri=uri+"&reviewer="+form.reviewer
    }


  this.soutenanceService.search(uri).subscribe(
   ( soutenances)=>{
      this.soutenances=soutenances.data;
     this.totalsize=soutenances.totalCount; 
    },(error)=>{
        console.log("err"+error);    
    })
}

clearSearch(form){
  form.reset()
  this.getListSoutenances(1,10)
}




  getListSoutenances(page,limit){
    this.soutenanceService.getListSoutenances(page,limit).subscribe((soutenances)=>{
      this.soutenances=soutenances.data;
      this.totalsize=soutenances.totalCount
      this.isLoading=false
    },(error)=>{
      console.log("error"+error);
      
    })
  }

  changeYear(year:AcademicYearEntity){
    this.yearSelected = year;
  }

  openRapport(url: string){
    window.open(url, "_blank");
}

downloadPdf(id,name,firstname){
  this.soutenanceService.downloadPdf(id).subscribe((result)=>{
    console.log(result.data);    
    let url = window.URL.createObjectURL(result);
    //window.open(url)
    console.log(url);
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = "soutenance-"+name+"-"+firstname+".pdf";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  })}


formattedSessionDate(date) {
  return date.replace(/T/, ' ').replace(/\..+/, '').replace(/-/, '/').replace(/-/, '/')
}

downloadRapport(id,name,firstname){
  this.internshipService.downloadRapport(id).subscribe((result)=>{
    console.log(result.data);    
    let url = window.URL.createObjectURL(result);
    //window.open(url)
    console.log(url);
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = "rapport-"+name+"-"+firstname+".pdf";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  })
}


submitDate(){
  console.log(this.selectedDate)
  if(this.selectedDate!=undefined){
  console.log(new Date(this.selectedDate).toLocaleString().replace(/,/, ''))
   this.timeIsChosen=true
   this.roomService.getAvailableRooms(this.selectedDate).subscribe(rooms =>
     {
       this.rooms=rooms.rooms;
       console.log(this.rooms)
     })
     this.soutenanceService.getTeacherThatCanBeJury(this.selectedDate).subscribe(teachers =>
       {
         console.log(teachers)
         this.jurys=teachers
       })
     
       this.sessionService.getListSessions(1,100).subscribe(sessions=>
             {
               this.sessions=sessions.data;
               this.session=this.sessions[0]
               for (var val of this.allSessions) {

                 if (val.startDate < this.selectedDate && val.endDate > this.selectedDate ){
                   this.session=val;
                   break;
                 }
             }
           }
         )

  } 
}

submitStudent(inscription_number){
this.studentService.searchByCriteria("inscription_number",inscription_number).subscribe
(student =>
 {
   console.log(student)
   this.student=student?.data[0]
   console.log(this.student)
   this.section=this.student?.section
   this.branch=this.student?.branch

   this.studentFetched=true
   this.studentService.getStudentInternshipsById(this.student?.id).subscribe
   (internships =>
    {
      this.internships=internships.interships
     console.log(internships)
    })

  

 })
}

OnOptionSelected1(value){
 this.fInternship=value

 this.session=this.sessions[0]
 for (var val of this.internships) {
   if (val.id ==this.fInternship ){
     this.sup1=val?.schoolSupervisor1?.name + " "+ val?.schoolSupervisor1?.firstname ;
     this.sup2=val?.schoolSupervisor2?.name + " "+ val?.schoolSupervisor2?.firstname ;
     break;
   }
 }
 this.internshipFetched=true

}

OnOptionSelected2(value){
 this.fPresident=value
}

OnOptionSelected3(value){
 this.fReviewer=value
}
OnOptionSelected4(value){
 this.fRoom=value
}

openModal( modal ) {
 this.modalService.open( modal, { size : 'lg' } );
}

addSoutenance(form:any,modal) {
 let dt= new Date(this.selectedDate)

 this.soutenanceToAdd= {
   "internship": "",
   "president": "",
   "reviewer": "",
   "soutenanceStartTime": "",
   "soutenanceEndTime": "",
   "room": "",
   "session": ""
 }
 this.soutenanceToAdd.internship=form.internship;
 this.soutenanceToAdd.president=form.president
 this.soutenanceToAdd.reviewer=form.reviewer
 this.soutenanceToAdd.soutenanceStartTime=form.soutenanceStartTime
 this.soutenanceToAdd.soutenanceEndTime=form.soutenanceEndTime

 this.soutenanceToAdd.room=form.room
 this.soutenanceToAdd.session=this.session?.id
 
 console.log(this.soutenanceToAdd)
   this.soutenanceService.addSoutenance(this.soutenanceToAdd).subscribe((response)=>{
   console.log("Ajouter Soutenance"+response);
   this.modalService.dismissAll()
   this.modalService.open( modal, { size : 'lg' } );
   }) 
}


}
