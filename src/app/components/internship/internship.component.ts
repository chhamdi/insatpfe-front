import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InternshipService } from 'src/app/services/internship.service';

@Component({
  selector: 'app-internship',
  templateUrl: './internship.component.html',
  styleUrls: ['./internship.component.scss']
})
export class InternshipComponent implements OnInit {
  
  internships : any
  type:any
  validate:Boolean
  isLoading:Boolean
  totalsize=0

  constructor(private internshipService: InternshipService,private route: ActivatedRoute ) {
    this.validate=false;
    this.isLoading=true
  }

 ngOnInit(): void {
  this.route.paramMap
  .subscribe(params => {
    this.type = params.get('type');
    this.internships = this.getListInternships(1,10)
      });
 }

 getListInternships(page,limit){
  return this.internshipService.getListInternship(page,10,this.type).subscribe(
    (response)=>{
      this.internships=response.data;
      console.log(this.type)
      console.log(this.internships)
      this.totalsize=response.totalCount; 
      this.isLoading=false
    },(error)=>{
        console.log("err"+error);   
        this.isLoading=false   
    })
} 



 search(form,page,limit){
  console.log(form)

    let uri:string="?page="+page+"&limit="+limit
    if (form.student!=""){
      uri=uri+"&student="+form.student
    }
    if (form.topic!=""){
      uri=uri+"&topic="+form.topic
    }
    if (form.society!=""){
      uri=uri+"&society="+form.society
    }
    if (form.supervisor!=""){
      uri=uri+"&supervisor="+form.supervisor
    }
    if (form.state!=""){
      uri=uri+"&state="+form.state
    }

    if(this.type=="ingenieurie"){
      uri=uri+"&section=ingenieurie"
    }
    
    if(this.type=="mastere"){
      uri=uri+"&section=mastere"
    }

    if(this.type=="licence"){
      uri=uri+"&section=licence"
    }

    if(this.type=="valider"){
      uri=uri+"&state=valid"
    }

  this.internshipService.search(uri).subscribe(
   ( teachers)=>{
      this.internships=teachers.data;
     this.totalsize=teachers.totalCount; 
    },(error)=>{
        console.log("err"+error);    
    })
}

clearSearch(form){
  form.reset()
  this.getListInternships(1,10)
}

 validateInternship(id){
  this.internshipService.validateInternship(id);
 }

 refuseInternship(id){
  this.internshipService.refuseInternship(id);
}

}
