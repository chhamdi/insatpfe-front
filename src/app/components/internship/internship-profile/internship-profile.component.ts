import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { InternshipService } from 'src/app/services/internship.service';
import { environment } from 'src/environments/environment';
import { StateEnum } from '../../../entities/enums/stage-state.enum';

@Component({
  selector: 'app-internship-profile',
  templateUrl: './internship-profile.component.html',
  styleUrls: ['./internship-profile.component.scss']
})
export class InternshipProfileComponent implements OnInit {

  uploader: FileUploader;
  internshipId :String;
  internship:any;
 
  internships: any;
  soutenances: any;

  isLoading:boolean
  waiting:boolean
  uploading : boolean = false ;
  constructor(private route: ActivatedRoute,private internshipService: InternshipService,private router: Router ) { 
    this.isLoading=true
    this.waiting=false
  }

  ngOnInit(): void {
    this.route.paramMap
    .subscribe(params => {
      this.internshipId = params.get('id');
      this.internshipService.getInternshipById(this.internshipId)
      .subscribe(
        response => {
        this.internship=response; 
        console.log("ssssss",this.internship.state)
        if(this.internship.state==StateEnum.WAITING){this.waiting=true}
        else {this.waiting=false}
        console.log(this.internship.state)
        this.isLoading=false
        console.log(this.internship) 

        })
        });      
    this.initFileUploader();

}

initFileUploader(): void {
  this.uploader = this.createUploader(`stages/${this.internshipId}/rapport/upload`);
}

createUploader(url: string): FileUploader {
    const token = localStorage.getItem('token');
    const uploader = new FileUploader(
      {
        url: environment.apiUrl + url,
        authToken: `Bearer ${token}`
      });

    //uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
    //  const year = this.years['data'].find(y => y.name == this.yearSelected)
    //  form.append('academicYear', year.id);
    //};

    uploader.onAfterAddingFile = (file) => {
      this.uploading = true ;
      file.withCredentials = false;
    };

    uploader.onSuccessItem = (item, response, status) => {
      if(status == 201)
      {
          this.uploading = false ;
      }
    };
    return uploader;
  }

validateInternship(){
  this.internshipService.validateInternship(this.internshipId).subscribe((response)=>{
    console.log(response);
    this.router.navigate(['/stages/profil', this.internshipId ])

    })
 }

 refuseInternship(){
  this.internshipService.refuseInternship(this.internshipId).subscribe((response)=>{
    console.log(response);
    this.router.navigate(['/stages/profil', this.internshipId ])
    })
}

uploadRapport(){
    console.log("ID"+this.internshipId);
    this.uploader.uploadAll();
    
 }

 downloadReport(){
    this.internshipService.downloadRapport( this.internship.id).subscribe((result)=>{
      console.log(result.data);    
      let url = window.URL.createObjectURL(result);
      //window.open(url)
      console.log(url);
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = "rapport-"+ this.internship?.student?.name+"-"+this.internship?.student?.firstname+".pdf";
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    })
}

}