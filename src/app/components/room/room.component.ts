import { Component, ElementRef, OnInit, SystemJsNgModuleLoader, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RoomDto } from 'src/app/dto/room.dto';
import { RoomService } from 'src/app/services/room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  
  rooms :any
  isLoading:Boolean
  selectedRoom : any;
  totalsize=0
  searchName:string

  constructor(private roomService: RoomService,    private modalService: NgbModal ) {
    this.isLoading=true
   }

  ngOnInit(): void {
    this.getListRooms(1,10);
    }

  getListRooms(page,limit){
    this.roomService.getListSalles(page,limit).subscribe(
      (rooms)=>{
        this.rooms=rooms.data;
        this.totalsize=rooms.totalCount; 
        this.isLoading=false
      },
      (error)=>{
          console.log("err"+error);  
      })
    }

    openModalDeleteRoom( modal ,room) {
      this.modalService.open( modal, { size : 'lg' } );
      this.selectedRoom = room;
    }
  
    deleteRoom(id:string, modal){
      this.roomService.deleteRoom(id).subscribe((response)=>{
        if(response.affected == 1){
          this.modalService.dismissAll()
          this.modalService.open( modal, { size : 'lg' } );
          this.getListRooms(1,10);
        }
      console.log(response);
      })
    }
  
    addRoom(formulaire:RoomDto,modal){
      this.roomService.addRoom(formulaire).subscribe((response)=>{
      console.log("room"+response);
      this.modalService.dismissAll()
       this.modalService.open( modal, { size : 'lg' } );
       this.getListRooms(1,10);
      })   
    }

    updateRoom(id:string,formulaire:RoomDto,modal){
      this.roomService.updateRoom(id,formulaire).subscribe((response)=>{
      console.log("room"+response);
      this.modalService.dismissAll()
       this.modalService.open( modal, { size : 'lg' } );
       this.getListRooms(1,10);
      })   
    }

    openModalAddRoom( modal ) {
      this.modalService.open( modal, { size : 'lg' } );
    }  

    openModalUpdateRoom( modal ,room : RoomDto) {
      this.modalService.open( modal, { size : 'lg' } );
      this.selectedRoom = room;
    }

    search(form,page,limit){
      this.roomService.search(form.name,page,limit).subscribe(
        (rooms)=>{
          this.rooms=rooms.data;
          this.totalsize=rooms.totalCount; 
        },(error)=>{
            console.log("err"+error);   
        })
    }

    clearSearch(form){
      form.reset()
      this.getListRooms(1,10) 
    }     
}
