import { Component, OnInit } from '@angular/core';
import { InternshipService } from 'src/app/services/internship.service';
import { SoutenanceService } from 'src/app/services/soutenance.service';

@Component({
  selector: 'app-rapports',
  templateUrl: './rapports.component.html',
  styleUrls: ['./rapports.component.scss']
})
export class RapportsComponent implements OnInit {

  
  internships : any;
  isLoading:Boolean
  totalsize=0

  constructor(private internshipService: InternshipService,
) {
    this.isLoading=true
  }

 ngOnInit(): void {
   this.getListRapports(1,10);
 }


  getListRapports(page,limit){
    this.internshipService.getListRapports(page,limit).subscribe((internships)=>{
      this.internships=internships.data;
      this.totalsize=internships.totalCount
      console.log(this.internships)
      this.isLoading=false
    },(error)=>{
      console.log("error"+error);
      this.isLoading=false   
    })
  }

  openRapport(url: string){
    window.open(url, "_blank");
}

downloadRapport(id,name,firstname){
  this.internshipService.downloadRapport(id).subscribe((result)=>{
    console.log(result.data);    
    let url = window.URL.createObjectURL(result);
    //window.open(url)
    console.log(url);
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = "rapport-"+name+"-"+firstname+".pdf";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  })
}

/*search(form,page,limit){
  console.log(form)
  let uri:string="?page="+page+"&limit="+limit

  if (form.topic!=""){
    uri=uri+"&topic="+form.topic
  }
   
  this.internshipService.search(uri).subscribe(
   ( internships)=>{
      this.internships=internships.data;
     this.totalsize=internships.totalCount; 
    },
    (error)=>{
        console.log("err"+error);    
    })
}*/
search(form,page,limit){
  console.log(form)

    let uri:string="?page="+page+"&limit="+limit
    if (form.student!=""){
      uri=uri+"&student="+form.student
    }
    if (form.topic!=""){
      uri=uri+"&topic="+form.topic
    }
    if (form.society!=""){
      uri=uri+"&society="+form.society
    }
    if (form.supervisor!=""){
      uri=uri+"&supervisor="+form.supervisor
    }
  this.internshipService.search(uri).subscribe(
   ( teachers)=>{
      this.internships=teachers.data;
     this.totalsize=teachers.totalCount; 
    },(error)=>{
        console.log("err"+error);    
    })
}


clearSearch(form){
  form.reset()
  this.getListRapports(1,10) 
}
}
