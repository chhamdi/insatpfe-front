import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminDto } from 'src/app/dto/admin.dto';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  admins: any
  isLoading: Boolean
  totalsize = 0
  public role: string;
  selectedAdmin: AdminDto;

  constructor(private adminService: AdminService, private modalService: NgbModal) {
    this.isLoading = true
  }

  ngOnInit(): void {
    this.role = localStorage.getItem('role');
    this.getListAdmins(1, 10);
  }


  getListAdmins(page, limit) {
    this.adminService.getListAdmins(page, limit).subscribe((admins) => {
      this.admins = admins.data;
      this.totalsize = admins.totalCount;
      this.isLoading = false
    })
  }





  openModalAddAdmin(modal) {
    this.modalService.open(modal, { size: 'lg' });
  }

  openModalDeleteAdmin(modal, Admin: AdminDto) {
    this.modalService.open(modal, { size: 'lg' });
    this.selectedAdmin = Admin;
  }


  openModalUpdateAdmin(modal, Admin: AdminDto) {
    this.modalService.open(modal, { size: 'lg' });
    this.selectedAdmin = Admin;
  }

  addAdmin(formulaire: AdminDto, modal) {
    let password = Math.random().toString(36).slice(-8);
    formulaire.password = password + "";
    formulaire.cin = parseInt(formulaire.cin + "");
    formulaire.passeport = parseInt(formulaire.passeport + "");
    formulaire.telephone = parseInt(formulaire.telephone + "")
    this.adminService.addAdmin(formulaire).subscribe((Admin) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getListAdmins(1, 10);
    })
  }
  updateAdmin(id: string, formulaire: AdminDto, modal) {
    formulaire.password = this.selectedAdmin.password;
    this.adminService.updateAdmin(id, formulaire).subscribe((response) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getListAdmins(1, 10);
    })
  }
  deleteAdmin(id: string, modal) {
    this.adminService.deleteAdmin(id).subscribe((response) => {
      this.modalService.dismissAll()
      this.modalService.open(modal, { size: 'lg' });
      this.getListAdmins(1, 10);
    })
  }


}
