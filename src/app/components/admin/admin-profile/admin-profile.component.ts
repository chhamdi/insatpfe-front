import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {

  adminId :String;
  admin:any;
  isLoading:boolean

  constructor(private route: ActivatedRoute,private adminService: AdminService,) { }

  ngOnInit(): void {
    this.route.paramMap
    .subscribe(params => {
      this.adminId = params.get('id');
      this.adminService.getAdminById(this.adminId)
      .subscribe(
        response => {
        this.admin=response; 
        this.isLoading=false
        console.log(this.admin) 
  })
})
  
}

}
