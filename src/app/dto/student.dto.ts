import { AcademicYearDto } from "./academicYear.dto";
import { UserDto } from './user.dto';



export class StudentDto extends UserDto {

  
  constructor(username: string, 
    email: string,
    password: string,
    name: string,
    firstname: string,
    cin: number,
    passeport: number,
    telephone: number,
    inscription_number: number,
    section: string,
    branch: string,
   academicYear: number,
    ){

    super(username, 
      email,
      password,
      name,
      firstname,
      cin,
      passeport,
      telephone)
      this.inscription_number=inscription_number,
      this.section=section,
      this.branch=branch
  }



  inscription_number: number;
  section: string;
  branch: string;
  academicYear: number;
}