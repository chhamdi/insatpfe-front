import { UserDto } from "./user.dto";

export class TeacherDto extends UserDto {

  
  constructor(username: string, 
    email: string,
    password: string,
    name: string,
    firstname: string,
    cin: number,
    passeport: number,
    telephone: number,
    ){
      
    super(username, 
      email,
      password,
      name,
      firstname,
      cin,
      passeport,
      telephone)
  }
}