import { AcademicYearDto } from './academicYear.dto';

export class SessionDto {

  constructor(  
    startDate: Date,
    endDate: Date,
    nom:String,
    academicYear: AcademicYearDto){
      this.startDate=startDate;
      this.endDate=endDate;
      this.academicYear= academicYear;
  }
  startDate: Date;
  endDate: Date;
  academicYear: AcademicYearDto;
  nom:String;

}