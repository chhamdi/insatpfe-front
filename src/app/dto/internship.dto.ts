import { StudentDto } from './student.dto';
import { TeacherDto } from './teacher.dto';


export class InternshipDto {


  constructor(
  student: StudentDto,
  topic: string,
  report: string,

  society: string,

  societySupervisor: string,

  schoolSupervisor1:TeacherDto,
  schoolSupervisor2:TeacherDto,

  startDate: Date,
  endDate: Date,
  ){

  this.student=student,
  this.topic=topic,
  this.report=report,

  this.society=society,

  this.societySupervisor=societySupervisor,

  this.schoolSupervisor1=schoolSupervisor1,
  this.schoolSupervisor2=schoolSupervisor2,

  this.startDate=startDate,
  this.endDate=endDate
  }

  student: StudentDto;
  topic: string;
  report: string;

  society: string;

  societySupervisor: string;

  schoolSupervisor1:TeacherDto;
  schoolSupervisor2:TeacherDto;

  startDate: Date;
  endDate: Date;
  state: string;
}