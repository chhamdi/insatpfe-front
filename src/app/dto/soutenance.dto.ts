import { InternshipDto } from './internship.dto';
import { RoomDto } from './room.dto';
import { SessionDto } from './session.dto';
import { TeacherDto } from './teacher.dto';

export class SoutenanceDto {

    constructor(  
    session: SessionDto, 
    internship: InternshipDto,

    president: TeacherDto,
    reviewer: TeacherDto,
   
    soutenanceStartTime: Date,
    soutenanceEndTime: Date,
    room: RoomDto,){
        this.session=session;  
        this.internship=internship;
    
        this.president=president;
        this.reviewer=reviewer;
    
        this.schoolSupervisor1=internship.schoolSupervisor1;
        this.schoolSupervisor2=internship.schoolSupervisor2;
    
        this.soutenanceStartTime=soutenanceStartTime;
        this.soutenanceEndTime=soutenanceEndTime;
        this.room=room;
    }

    session: SessionDto;  
    internship: InternshipDto;

    president: TeacherDto;
    reviewer: TeacherDto;

    schoolSupervisor1: TeacherDto;
    schoolSupervisor2: TeacherDto;

    soutenanceStartTime: Date;
    soutenanceEndTime: Date;
    room: RoomDto;
    
}