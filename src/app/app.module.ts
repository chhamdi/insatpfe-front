
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule, ThemeService } from 'ng2-charts';
import { FileUploadModule } from 'ng2-file-upload';


import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { ContentAnimateDirective } from './shared/directives/content-animate.directive';
import { CommonModule } from '@angular/common';

import { InternshipComponent } from './components/internship/internship.component';
import { TeacherComponent } from './components/teacher/teacher.component';
import { StudentComponent } from './components/student/student.component';
import { SessionComponent } from './components/session/session.component';
import { SoutenanceComponent } from './components/soutenance/soutenance.component';
import { RoomComponent } from './components/room/room.component';
import { AcademicYearComponent } from './components/academic-year/academic-year.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { StudentProfileComponent } from './components/student/student-profile/student-profile.component';
import { TeacherProfileComponent } from './components/teacher/teacher-profile/teacher-profile.component';
import { InternshipProfileComponent } from './components/internship/internship-profile/internship-profile.component';
import { SoutenanceProfileComponent } from './components/soutenance/soutenance-profile/soutenance-profile.component';
import { SessionProfileComponent } from './components/session/session-profile/session-profile.component';
import { AuthComponent } from './components/auth/auth.component';
import { RapportsComponent } from './components/rapports/rapports.component';
import { AdminComponent } from './components/admin/admin.component';
import { AdminProfileComponent } from './components/admin/admin-profile/admin-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    SpinnerComponent,
    ContentAnimateDirective,
    InternshipComponent,
    TeacherComponent,
    StudentComponent,
    SessionComponent,
    SoutenanceComponent,
    RoomComponent,
    AcademicYearComponent,
    AuthComponent,
    StudentProfileComponent,
    TeacherProfileComponent,
    SessionProfileComponent,
    SoutenanceProfileComponent,
    InternshipProfileComponent,
    AdminComponent,
    AdminProfileComponent,
    RapportsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    CommonModule,
    HttpClientModule,
    FileUploadModule
   

  ],
  providers: [ThemeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
