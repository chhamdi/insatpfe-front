import { SoutenanceEntity } from './Soutenance.entity';


  export class RoomEntity {

    id: string;
  
    name:string;

    soutenancesList: SoutenanceEntity;

}