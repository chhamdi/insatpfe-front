import { AcademicYearEntity } from './AcademicYear.entity';
import { SoutenanceEntity } from './Soutenance.entity';


  export class SessionEntity {

    id: number;
  
    startDate: Date;
    endDate: Date;

    soutenanceList: SoutenanceEntity [];

    academicYear: AcademicYearEntity;
  
}