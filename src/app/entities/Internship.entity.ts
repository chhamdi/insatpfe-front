import { TeacherEntity } from './Teacher.entity';
import { StudentEntity } from './Student.entity';
import { SoutenanceEntity } from "./Soutenance.entity";
import { AcademicYearEntity } from './AcademicYear.entity';

export class InternshipEntity {

  id: number;

  student: StudentEntity;

  soutenance: SoutenanceEntity;

  topic: string;
  report: string;

  society: string;

  societySupervisor: string;


  schoolSupervisor1:TeacherEntity;
  schoolSupervisor2:TeacherEntity;

  startDate: Date;
  endDate: Date;

  state: string;
  academicYear : AcademicYearEntity

}