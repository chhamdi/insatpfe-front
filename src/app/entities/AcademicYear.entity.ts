import { SessionEntity } from './Session.entity';
import { SoutenanceEntity } from './Soutenance.entity';
import { StudentEntity } from './Student.entity';


  export class AcademicYearEntity {

    id: number;
    name : string;

    sessionList: SoutenanceEntity [];
    studentList:StudentEntity[];
  
}