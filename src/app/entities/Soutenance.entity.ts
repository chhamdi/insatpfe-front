import { TeacherEntity } from './Teacher.entity';
import { SessionEntity } from "./Session.entity";
import { RoomEntity } from './Room.entity';
import { AcademicYearDto } from '../dto/academicYear.dto';
import { AcademicYearEntity } from './AcademicYear.entity';



  export class SoutenanceEntity {
  
    id: string;

    session: SessionEntity;  

    president: TeacherEntity;
    reviewer: TeacherEntity;

    soutenanceStartTime: Date;
    soutenanceEndTime: Date;

    room: RoomEntity;
    academicYear : AcademicYearEntity
}