import { UserEntity } from './User.entity';
import { InternshipEntity} from './Internship.entity';
import { AcademicYearEntity } from './AcademicYear.entity';


  export class StudentEntity extends UserEntity{
    
    constructor(){
    super();
    this.inscription_number=16000457,
    this.section="ingenieurie",
    this.branch="GL"
    //academicYear: AcademicYearDto,
  }


    inscription_number: number;

    section: string;
    branch: string;

    internshipList: InternshipEntity[];
    academicYear: AcademicYearEntity;
  
  }

