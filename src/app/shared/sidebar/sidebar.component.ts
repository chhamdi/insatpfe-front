import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public uiBasicCollapsed = false;
  public samplePagesCollapsed = false;
  public role : string ;
  public name : string ;
  
  constructor(private router : Router) {
   }

  ngOnInit() {
    this.role = localStorage.getItem('role');
    this.name = localStorage.getItem('firstname');
    console.log("role"+this.role);
    
    const body = document.querySelector('body');

    // add class 'hover-open' to sidebar navitem while hover in sidebar-icon-only menu
    document.querySelectorAll('.sidebar .nav-item').forEach(function (el) {
      el.addEventListener('mouseover', function() {
        if(body.classList.contains('sidebar-icon-only')) {
          el.classList.add('hover-open');
        }
      });
      el.addEventListener('mouseout', function() {
        if(body.classList.contains('sidebar-icon-only')) {
          el.classList.remove('hover-open');
        }
      });
    });
  }


  redirectToProfile(){

    let id = localStorage.getItem('id');
    if(localStorage.getItem('role')=="admin")
        this.router.navigate(['/profile/admin/'+id]);
    else if(localStorage.getItem('role')=="etudiant")
        this.router.navigate(['/profile/etudiant/'+id])
    else
        this.router.navigate(['/profile/enseignant/'+id])


  }

}
