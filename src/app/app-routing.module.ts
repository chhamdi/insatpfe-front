import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InternshipComponent } from './components/internship/internship.component';
import { TeacherComponent } from './components/teacher/teacher.component';
import { StudentComponent } from './components/student/student.component';
import { SessionComponent } from './components/session/session.component';
import { SoutenanceComponent } from './components/soutenance/soutenance.component';
import { RoomComponent } from './components/room/room.component';
import { AcademicYearComponent } from './components/academic-year/academic-year.component';
import { StudentProfileComponent } from './components/student/student-profile/student-profile.component';
import { TeacherProfileComponent } from './components/teacher/teacher-profile/teacher-profile.component';
import { SessionProfileComponent } from './components/session/session-profile/session-profile.component';
import { SoutenanceProfileComponent } from './components/soutenance/soutenance-profile/soutenance-profile.component';
import { InternshipProfileComponent } from './components/internship/internship-profile/internship-profile.component';
import { AuthComponent } from './components/auth/auth.component';
import { AdminProfileComponent } from './components/admin/admin-profile/admin-profile.component';
import { AdminComponent } from './components/admin/admin.component';
import { RapportsComponent } from './components/rapports/rapports.component';


const routes: Routes = [
  {
    path:'login',component:AuthComponent
  },
  { path: 'annee_universitaires',  component: AcademicYearComponent },
  { path: 'enseignants', 
  children: [
    { 
      path: 'profil/:id', 
      component: TeacherProfileComponent
     },
    { 
      path: '', 
      component: TeacherComponent 
    } 
  ]
  },
  {path: 'etudiants', 
    children: [
      { 
        path: 'profil/:id', 
        component: StudentProfileComponent
       },
      { 
        path: '', 
        component: StudentComponent 
      }
    ]
  },
  { path: 'stages', children: [
    { 
      path: 'profil/:id', 
      component: InternshipProfileComponent
     },
    { 
      path: ':type', 
      component: InternshipComponent 
    }
  ]},
  { path: 'sessions', 
  children: [
    { 
      path: 'profil/:id', 
      component: SessionProfileComponent
     },
    { 
      path: '', 
      component: SessionComponent 
    }
  ]},
  { path: 'soutenances', children: [
    { 
      path: 'profil/:id', 
      component: SoutenanceProfileComponent
     },
    { 
      path: '', 
      component: SoutenanceComponent 
    }
  ] },
  { path: 'salles', component: RoomComponent},
  { path: 'admins', children: [
    { 
      path: 'profil/:id', 
      component: AdminProfileComponent
     },
    { 
      path: '', 
      component: AdminComponent 
    }
  ]},
  { path: 'rapports',  component: RapportsComponent  },
  { path: 'profile/etudiant/:id',  component:StudentProfileComponent},
  { path: 'profile/enseignant/:id',  component: TeacherProfileComponent  },
  { path: 'profile/admin/:id',  component: AdminProfileComponent  },
  { path: '',  component: RoomComponent  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{anchorScrolling: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
