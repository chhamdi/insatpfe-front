import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable,of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AcademicYearService {

  getAcademicYearStudentsById(id) {
    return this.http.get(environment.apiUrl+this.base_acYear+'/'+id+'/students');
  }

  getAcademicYearById(id){
    return this.http.get(environment.apiUrl+this.base_acYear+'/'+id);
  }



  base_acYear = "annees_universitaires";

  constructor(private http: HttpClient) {
   }

   getListAcadameciYears(page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_acYear}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched academic years")),
      catchError(this.handleError("getAcadameciYears", []))
    );
   }

   private log(message: string) {
    console.log(message);
  }

   private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getStudentsByAcYear(id,page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_acYear}/${id}/etudiants?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched students ac years")),
      catchError(this.handleError("getAcadameciYears studens", []))
    );
   
  }

  addAcademicYear(AcademicYear):Observable<any>{
    return this.http.post(environment.apiUrl+this.base_acYear,AcademicYear).pipe(
     tap((_) => this.log("added year")),
     catchError(this.handleError("deleteyear", []))
   );
  }

  deleteAcademicYear(id):Observable<any>{

   return this.http.delete(environment.apiUrl+this.base_acYear+"/"+id).pipe(
     tap((_) => this.log("deleted year")),
     catchError(this.handleError("deleteyear", []))
   );
  }

  updateAcademicYear(id,AcademicYear):Observable<any>{
   return this.http.put(environment.apiUrl+this.base_acYear+"/"+id,AcademicYear).pipe(
    tap((_) => this.log("updated year")),
    catchError(this.handleError("updateyear", []))
  );
  }

  search(name,page,limit):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_acYear+"/recherche/name/"+name+"?page="+page+"&limit="+limit).pipe(
      tap((_) => this.log("fetched years")),
      catchError(this.handleError("getYears", []))
    );
   }


}
