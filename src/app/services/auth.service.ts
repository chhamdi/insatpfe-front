import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  auth_url = "user/login"
  constructor(private http : HttpClient) { }
    //login user
    login(crendentials): Observable<any> {
      return this.http
        .post<any>(environment.apiUrl+ this.auth_url, crendentials)
        .pipe(
          tap((_) => this.log("login")),
          catchError(this.handleError("login", []))
        );
    }


  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('id');
  }

    private log(message: string) {
      console.log(message);
    }
  
    private handleError<T>(operation = "operation", result?: T) {
      return (error: any): Observable<T> => {
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead
  
        // TODO: better job of transforming error for user consumption
        this.log(`${operation} failed: ${error.message}`);
  
        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
}
