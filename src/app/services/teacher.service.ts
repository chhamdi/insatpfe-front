import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";
import { TeacherDto } from '../dto/teacher.dto';
import { TeacherEntity } from '../entities/Teacher.entity';




const API_Link = 'https://insat-pfe.herokuapp.com/enseignants'


@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  teachers: TeacherDto[] = [];
  teacher: TeacherDto;
  base_teachers = "enseignants"

  constructor(private http: HttpClient) {
  }

  getListTeachers(page, limit): Observable<any> {
    return this.http.get(`${environment.apiUrl}${this.base_teachers}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched teachers")),
      catchError(this.handleError("getTechers", []))
    );
  }

  addTeacher(teacher): Observable<any> {
    return this.http.post(environment.apiUrl + this.base_teachers, teacher).pipe(
      tap((_) => this.log("deleted teachers")),
      catchError(this.handleError("deleteTeacher", []))
    );
  }

  deleteTeacher(id): Observable<any> {

    return this.http.delete(environment.apiUrl + this.base_teachers + "/" + id).pipe(
      tap((_) => this.log("deleted teachers")),
      catchError(this.handleError("delete", []))
    );
  }
  updateTeacher(id, teacher): Observable<any> {
    return this.http.put(environment.apiUrl + this.base_teachers + "/" + id, teacher).pipe(
      tap((_) => this.log("updated teachers")),
      catchError(this.handleError("updateTeachers", []))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getTeacherById(id): Observable<any> {
    return this.http.get(API_Link + '/' + id);
  }

  getTeacherInternshipsById(id) {
    return this.http.get(API_Link + '/' + id + '/stages');
  }

  getTeacherSoutenancesById(id) {
    return this.http.get(API_Link + '/' + id + '/soutenances');
  }

  getTeacherPresidentSoutenancesById(id) {
    return this.http.get(API_Link + '/' + id + '/soutenances/president');
  }

  getTeacherReviewerSoutenancesById(id) {
    return this.http.get(API_Link + '/' + id + '/soutenances/examinateur');
  }

  getTeachers(name) {
    return this.http.get(API_Link + '/rechercheParNomOuPrenom/' + name);
  }

  search(uri): Observable<any> {
    return this.http.get(environment.apiUrl + this.base_teachers + "/recherche/" + uri).pipe(
      tap((_) => this.log("fetched teachers")),
      catchError(this.handleError("getTeachers", []))
    );
  }
  
}
