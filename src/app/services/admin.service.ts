import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdminDto } from '../dto/admin.dto';


import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";




@Injectable({
  providedIn: 'root'
})
export class AdminService {

  admins: AdminDto[] = [];
  admin: AdminDto;
  base_admins = "admins"

  constructor(private http: HttpClient) {
  }

  getListAdmins(page, limit): Observable<any> {
    return this.http.get(`${environment.apiUrl}${this.base_admins}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched Admins")),
      catchError(this.handleError("getTechers", []))
    );
  }

  addAdmin(admin): Observable<any> {
    return this.http.post(environment.apiUrl + this.base_admins, admin).pipe(
      tap((_) => this.log("deleted Admins")),
      catchError(this.handleError("deleteAdmin", []))
    );
  }

  deleteAdmin(id): Observable<any> {

    return this.http.delete(environment.apiUrl + this.base_admins + "/" + id).pipe(
      tap((_) => this.log("deleted Admins")),
      catchError(this.handleError("delete", []))
    );
  }
  updateAdmin(id, admin): Observable<any> {
    return this.http.put(environment.apiUrl + this.base_admins + "/" + id, admin).pipe(
      tap((_) => this.log("updated Admins")),
      catchError(this.handleError("updateAdmins", []))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getAdminById(id): Observable<any> {
    return this.http.get(environment.apiUrl + this.base_admins + '/' + id);
  }

}
