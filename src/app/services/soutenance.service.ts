import { Injectable } from '@angular/core';
import { SessionDto } from '../dto/session.dto';
import { SoutenanceDto } from '../dto/soutenance.dto';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable,of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";






@Injectable({
  providedIn: 'root'
})
export class SoutenanceService {
   
  base_soutenance ="soutenances"
  constructor(private http: HttpClient) {}

   getListSoutenances(page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_soutenance}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched soutenances")),
      catchError(this.handleError("getsoutenances", []))
    );
   }

   addSoutenance(student):Observable<any>{
     return this.http.post(environment.apiUrl+this.base_soutenance,student).pipe(
      tap((_) => this.log("deleted soutenances")),
      catchError(this.handleError("deleteSsoutenances", []))
    );
   }

   deleteSoutenance(id):Observable<any>{

    return this.http.delete(environment.apiUrl+this.base_soutenance+"/"+id).pipe(
      tap((_) => this.log("deleted ssoutenances")),
      catchError(this.handleError("deletesoutenances", []))
    );
   }

   private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getSoutenanceById(id){
    return this.http.get(environment.apiUrl+this.base_soutenance+"/"+id);
  }

  getInternshipOfSoutenanceById(id: String) {
    return this.http.get(`${environment.apiUrl}+${this.base_soutenance}/${id}/stage`);
  }

downloadPdf(id):Observable<any>{
  return this.http.get(environment.apiUrl+this.base_soutenance+"/"+id+"/pdf",{
    responseType: 'blob'
}).pipe(
    tap((_) => this.log("download rapport")),
    catchError(this.handleError("error download rapport", []))
  );
}


getTeacherThatCanBeJury(date):Observable<any>{
  return this.http.get(environment.apiUrl+this.base_soutenance+"/"+date+"/possibleJury/").pipe(
    tap((_) => this.log("fetched teachers")),
    catchError(this.handleError("getTeachers", []))
  );
}

search(uri):Observable<any>{
  return this.http.get(environment.apiUrl+this.base_soutenance+"/recherche/"+uri).pipe(
    tap((_) => this.log("fetched soutenances")),
    catchError(this.handleError("getSoutenances", []))
  );
 }

}
