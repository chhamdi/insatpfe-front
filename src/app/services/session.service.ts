import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable,of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";
import { SessionDto } from '../dto/session.dto';



const API_Link='https://insat-pfe.herokuapp.com/sessions'


@Injectable({
  providedIn: 'root'
})
export class SessionService {
 
  base_sessions ="sessions"
  constructor(private http: HttpClient) {}

   getListSessions(page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_sessions}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched sessions")),
      catchError(this.handleError("getSessions", []))
    );
   }

   addSession(session):Observable<any>{
     return this.http.post(environment.apiUrl+this.base_sessions,session).pipe(
      tap((_) => this.log("deleted students")),
      catchError(this.handleError("deleteSessions", []))
    );
   }

   deleteSession(id):Observable<any>{

    return this.http.delete(environment.apiUrl+this.base_sessions+"/"+id).pipe(
      tap((_) => this.log("deleted students")),
      catchError(this.handleError("deleteStudents", []))
    );
   }

   private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getSessionById(id) {
    return this.http.get(environment.apiUrl+this.base_sessions+"/"+id);
  }

  getSessionSoutenancesById(id) {
    return this.http.get(environment.apiUrl+this.base_sessions+"/"+id+'/soutenances');
  }

  search(uri):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_sessions+"/recherche/"+uri).pipe(
      tap((_) => this.log("fetched sessions")),
      catchError(this.handleError("getSessions", []))
    );
  }
  
  }



  