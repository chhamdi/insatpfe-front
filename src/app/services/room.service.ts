import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable,of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";
import { SessionDto } from '../dto/session.dto';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  base_salles ="salles"
  constructor(private http: HttpClient) {}

   getListSalles(page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_salles}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched salles")),
      catchError(this.handleError("getSalles", []))
    );
   }

   search(name,page,limit):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_salles+"/recherche/name/"+name+"?page="+page+"&limit="+limit).pipe(
      tap((_) => this.log("fetched salles")),
      catchError(this.handleError("getSalles", []))
    );
   }

   addRoom(room):Observable<any>{
     return this.http.post(environment.apiUrl+this.base_salles,room).pipe(
      tap((_) => this.log("added salles")),
      catchError(this.handleError("deletesalles", []))
    );
   }

   deleteRoom(id):Observable<any>{

    return this.http.delete(environment.apiUrl+this.base_salles+"/"+id).pipe(
      tap((_) => this.log("deleted salles")),
      catchError(this.handleError("deleteSalles", []))
    );
   }

   updateRoom(id,room):Observable<any>{
    return this.http.put(environment.apiUrl+this.base_salles+"/"+id,room).pipe(
      tap((_) => this.log("updated salles")),
      catchError(this.handleError("updateSalles", []))
    );
   }

   private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getAvailableRooms(date):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_salles+"/disponibles/"+date).pipe(
      tap((_) => this.log("fetched salles")),
      catchError(this.handleError("getSalles", []))
    );
  }
  
}
