import { Injectable } from '@angular/core';
import { StudentDto } from '../dto/student.dto';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";



const API_Link = 'https://insat-pfe.herokuapp.com/etudiants'



@Injectable({
  providedIn: 'root'
})
export class StudentService {

  students: StudentDto[] = [];
  student: StudentDto;
  base_students = "etudiants"

  constructor(private http: HttpClient) { }

  getListStudent(page, limit): Observable<any> {
    console.log(environment.apiUrl)
    return this.http.get(`${environment.apiUrl}${this.base_students}?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched students")),
      catchError(this.handleError("getStudents", []))
    );
  }

  addStudent(student): Observable<any> {
    return this.http.post(environment.apiUrl + this.base_students, student).pipe(
      tap((_) => this.log("deleted students")),
      catchError(this.handleError("deleteStudents", []))
    );
  }

  deleteStudent(id): Observable<any> {
    return this.http.delete(environment.apiUrl + this.base_students + "/" + id).pipe(
      tap((_) => this.log("deleted students")),
      catchError(this.handleError("deleteStudents", []))
    );
  }

  updateStudent(id, student): Observable<any> {
    return this.http.put(environment.apiUrl + this.base_students + "/" + id, student).pipe(
      tap((_) => this.log("updated Students")),
      catchError(this.handleError("updateStudents", []))
    );
  }

  private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  getStudentById(id): Observable<any> {
    return this.http.get(API_Link + '/' + id);
  }

  getStudentInternshipsById(id):Observable<any>{
    return this.http.get(API_Link+'/'+id+'/stages');
  }

  getStudentSoutenancesById(id) {
    return this.http.get(API_Link + '/' + id + '/soutenances');
  }

  search(uri): Observable<any> {
    return this.http.get(environment.apiUrl + this.base_students + "/recherche/" + uri).pipe(
      tap((_) => this.log("fetched students")),
      catchError(this.handleError("getStudents", []))
    );
  }

   searchByCriteria(criteria,term):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_students+"/recherche/"+criteria+"/"+term+"?page=1&limit=10").pipe(
      tap((_) => this.log("fetched students")),
      catchError(this.handleError("getStudents", []))
    );
   }

}
