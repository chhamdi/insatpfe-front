import { Injectable } from '@angular/core';
import { UserDto } from '../dto/user.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  
  users : UserDto[]=[];
  user:UserDto;

  constructor() {
    this.user =  new UserDto("16328846", 
      "jennyyosr@gmail.com",
      "12345",
      "Jenny",
      "Yosr",
      13628846,
      1000,
      99958590,
      )
      this.users .push(this.user);
   }

   getListUser():UserDto[]{
    
    return this.users;
   }
}
