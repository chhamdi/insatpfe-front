import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable,of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, tap } from "rxjs/operators";



const API_Link='https://insat-pfe.herokuapp.com/stages'


@Injectable({
  providedIn: 'root'
})
export class InternshipService {
  base_internship ="stages"
  constructor(private http: HttpClient) {}

   getListSoutenances(page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_internship}?page=1&limit=20`).pipe(
      tap((_) => this.log("fetched internship")),
      catchError(this.handleError(" get internship", []))
    );
   }

   addSoutenance(student):Observable<any>{
     return this.http.post(environment.apiUrl+this.base_internship,student).pipe(
      tap((_) => this.log("deleted internship")),
      catchError(this.handleError("deleteinternship", []))
    );
   }

   deleteSoutenance(id):Observable<any>{

    return this.http.delete(environment.apiUrl+this.base_internship+"/"+id).pipe(
      tap((_) => this.log("deleted ")),
      catchError(this.handleError("deleted internship", []))
    );
   }

   private log(message: string) {
    console.log(message);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getInternshipById(id) {
    return this.http.get(environment.apiUrl+this.base_internship+"/"+id);
  }

  getListInternship(page,limit,type):Observable<any>{

    if(type=="valider"){
      return this.http.get(`${environment.apiUrl}${this.base_internship}/enAttente?page=${page}&limit=${limit}`)
    }
    else if(type=="ingenieurie"){
      return this.http.get(`${environment.apiUrl}${this.base_internship}/ingenieur?page=${page}&limit=${limit}`)
    }
    else if(type=="licence"){
      return this.http.get(`${environment.apiUrl}${this.base_internship}/licence?page=${page}&limit=${limit}`) 
    }
    else if(type=="mastere"){
      return this.http.get(`${environment.apiUrl}${this.base_internship}/mastere?page=${page}&limit=${limit}`)
    }
  }

  validateInternship(id){
  return this.http.post(`${environment.apiUrl}${this.base_internship}/${id}/valider`,{});

  }

   refuseInternship(id){
    return this.http.post(`${environment.apiUrl}${this.base_internship}/${id}/refuser`,{})
  }

  downloadRapport(id):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_internship+"/"+id+"/rapport/download",{
      responseType: 'blob'
  }).pipe(
      tap((_) => this.log("download rapport")),
      catchError(this.handleError("error download rapport", []))
    );
  }

  /*uploadRapport(id):Observable<any>{

    //return this.http.post(environment.apiUrl+this.base_internship+"/"+id+"/rapport/upload",)
  }*/

  getListRapports(page,limit):Observable<any>{
    return this.http.get(`${environment.apiUrl}${this.base_internship}/rapports?page=${page}&limit=${limit}`).pipe(
      tap((_) => this.log("fetched rapports")),
      catchError(this.handleError(" get rapport", []))
    );
   }


   addInternship(internship):Observable<any>{
    return this.http.post(environment.apiUrl+this.base_internship,internship).pipe(
     tap((_) => this.log("added stage")),
     catchError(this.handleError("added stages error", []))
   );
  }

  search(uri):Observable<any>{
    return this.http.get(environment.apiUrl+this.base_internship+"/recherche/"+uri).pipe(
      tap((_) => this.log("fetched internships")),
      catchError(this.handleError("getInternships", []))
    );
   }


}